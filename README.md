# Instructions

This Project is used to reload URL from busy page to the target page.

  - You need to install the npm command
  - You need to install java command
  - You need to install Firefox
  - You need to install selenium-server-standalone-3.5.3.jar

Command:
  1. npm install
  2. java -jar -Dwebdriver.gecko.driver=./geckodriver selenium-server-standalone-3.5.3.jar
  3. node index


> Isaac NG, Ka Ho (HK)

> Web & Hybird App Developer

> Contact: 5kahoisaac@gmail.com
