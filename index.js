var webdriverio = require('webdriverio');
var options = {
  desiredCapabilities: {
    browserName: 'firefox'
  }
};

var i = 0;

function looper(browser, url) {
  if (url == "http://msg.urbtix.hk/") { // busy page url
    i++;
    console.log("Run " + i + "'s time");
    browser.url("http://www.urbtix.hk"). // target url
    getUrl().then(function(url) {
      looper(browser, url);
    });
  } else {
    console.log("success");
  }
}

var browser = webdriverio.remote(options).init().url("http://www.urbtix.hk"). // target url
getUrl().then(function(url) {
  looper(browser, url)
}).catch(function(err) {
  console.log(err);
})
